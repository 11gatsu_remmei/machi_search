import Query from './query.js';
import EmptyQuery from './empty_query.js';

function intersectionOfPositions(map1, map2) {
  const intersection = new Map();

  map1.forEach((positions1, speaker) => {
    if (map2.has(speaker)) {
      const positions2 = map2.get(speaker);
      const commonPositions = Array.from(positions1).filter(p =>
        positions2.has(p)
      );

      if (commonPositions.length > 0) {
        intersection.set(speaker, new Set(commonPositions));
      }
    }
  });

  return intersection;
}

/**
 * 2つの検索結果配列をマージして返す。
 *
 * 検索結果はソートされていると仮定する。
 */
function mergeResults(results1, results2) {
  const results = [];
  let i1 = 0;
  let i2 = 0;

  while (i1 < results1.length && i2 < results2.length) {
    const result1 = results1[i1];
    const result2 = results2[i2];

    const newGranularity = Math.min(
      result1.sortKey.length,
      result2.sortKey.length
    );

    const collapsed1 = result1.collapsed(newGranularity);
    const collapsed2 = result2.collapsed(newGranularity);

    const comparision = collapsed1.compareLocationTo(collapsed2);

    if (comparision === 0) {
      // Resultをコマの集合としてみたときに、
      // result1 ⊆ result2または result1 ⊇ result2である。

      // コマの集合としてみたときに相手に含まれるResult(collapseしていない)の
      // 配列。
      const occurences1 = [];
      const occurences2 = [];

      while (
        i1 < results1.length &&
        collapsed2.compareLocationTo(results1[i1].collapsed(newGranularity)) ===
          0
      ) {
        occurences1.push(results1[i1]);

        i1++;
      }

      while (
        i2 < results2.length &&
        collapsed1.compareLocationTo(results2[i2].collapsed(newGranularity)) ===
          0
      ) {
        occurences2.push(results2[i2]);

        i2++;
      }

      occurences1.forEach(o1 => {
        occurences2.forEach(o2 => {
          if (o1.sortKey.length === o2.sortKey.length) {
            if (o1.positions && o2.positions) {
              const intersection = intersectionOfPositions(
                o1.positions,
                o2.positions
              );

              if (intersection.size > 0) {
                results.push(
                  o1
                    .withPositions(intersection)
                    .withSpeakers(new Set(intersection.keys()))
                );
              }
            } else if (o1.positions) {
              results.push(o1);
            } else if (o2.positions) {
              results.push(o2);
            } else {
              results.push(o1.merge(o2));
            }
          } else if (o1.sortKey.length < o2.sortKey.length) {
            results.push(o2);
          } else {
            results.push(o1);
          }
        });
      });
    } else if (comparision < 0) {
      i1++;
    } else {
      i2++;
    }
  }

  return results;
}

function subtractResults(results1, results2) {
  // TODO mergeResultsと共通化する。
  const results = [];
  let i1 = 0;
  let i2 = 0;

  while (i1 < results1.length && i2 < results2.length) {
    const result1 = results1[i1];
    const result2 = results2[i2];

    const newGranularity = Math.min(
      result1.sortKey.length,
      result2.sortKey.length
    );

    const collapsed1 = result1.collapsed(newGranularity);
    const collapsed2 = result2.collapsed(newGranularity);

    const comparision = collapsed1.compareLocationTo(collapsed2);

    if (comparision === 0) {
      // Resultをコマの集合としてみたときに、
      // result1 ⊆ result2または result1 ⊇ result2である。

      // コマの集合としてみたときに相手に含まれるResult(collapseしていない)の
      // 配列。
      const occurences1 = [];
      const occurences2 = [];

      while (
        i1 < results1.length &&
        collapsed2.compareLocationTo(results1[i1].collapsed(newGranularity)) ===
          0
      ) {
        occurences1.push(results1[i1]);

        i1++;
      }

      while (
        i2 < results2.length &&
        collapsed1.compareLocationTo(results2[i2].collapsed(newGranularity)) ===
          0
      ) {
        occurences2.push(results2[i2]);

        i2++;
      }

      occurences1.forEach(o1 => {
        const haveIntersection = occurences2.filter(o2 => {
          if (
            o1.sortKey.length === o2.sortKey.length &&
            o1.positions &&
            o2.positions
          ) {
            const intersection = intersectionOfPositions(
              o1.positions,
              o2.positions
            );

            return intersection.size > 0;
          } else {
            return true;
          }
        });

        if (!haveIntersection) {
          results.push(o1);
        }
      });
    } else if (comparision < 0) {
      results.push(result1);
      i1++;
    } else {
      i2++;
    }
  }

  for (; i1 < results1.length; i1++) {
    results.push(results1[i1]);
  }

  return results;
}

/**
 * 複数の問い合わせ結果の全てにマッチする結果を返す問合せ。
 */
export default class AndQuery extends Query {
  /**
   * @param {Query[]} queries ベースとなる問い合わせ
   * @param {Query[]} negativeQueries 除外する問い合わせ
   */
  constructor(queries, negativeQueries) {
    super();

    this.queries = queries;
    this.negativeQueries = negativeQueries;
  }

  /**
   * 非同期的に検索して結果を返す。
   *
   * @param {SearchOption} options
   * @return {Promise[Result[]]}
   */
  async execute(options) {
    if (this.queries.length === 0) {
      return [];
    }

    const [matches, negativeMatches] = await Promise.all([
      Promise.all(this.queries.map(query => query.execute(options))),
      Promise.all(this.negativeQueries.map(query => query.execute(options))),
    ]);

    const merged = matches.reduce((left, right) => mergeResults(left, right));

    return negativeMatches.reduce(
      (left, right) => subtractResults(left, right),
      merged
    );
  }

  normalize() {
    const queries = this.queries
      .map(query => query.normalize())
      .filter(query => query !== EmptyQuery);
    const negativeQueries = this.negativeQueries
      .map(query => query.normalize())
      .filter(query => query !== EmptyQuery);

    if (queries.length === 0) {
      return EmptyQuery;
    } else if (queries.length === 1 && negativeQueries.length === 0) {
      return queries[0];
    } else {
      return new AndQuery(queries, negativeQueries);
    }
  }

  toString() {
    return (
      'and(' +
      this.queries
        .map(query => query.toString())
        .concat(this.negativeQueries.map(query => '-' + query.toString()))
        .join(', ') +
      ')'
    );
  }
}
