import Query from './query.js';
import EmptyQuery from './empty_query.js';

/**
 * 粒度がコマ単位以上の粗さの場合、同じlocationの結果を1つにまとめる。
 */
function mergeDuplication(results) {
  // positionsの有無は全要素共通であると仮定する。
  if (results.length > 0 && results[0].positions) {
    return results;
  }

  let lastResult = null;
  const merged = [];

  results.forEach(result => {
    if (
      lastResult &&
      lastResult.book === result.book &&
      lastResult.compareLocationTo(result) === 0
    ) {
      const newSpeakers = new Set(lastResult.speakers);

      for (const speaker of result.speakers) {
        newSpeakers.add(speaker);
      }

      lastResult = lastResult.withSpeakers(newSpeakers);
    } else {
      if (lastResult) {
        merged.push(lastResult);
      }

      lastResult = result;
    }
  });

  if (lastResult) {
    merged.push(lastResult);
  }

  return merged;
}

export default class GranularityQuery extends Query {
  constructor(query, granularity) {
    super();

    this.query = query;

    if (GranularityQuery.nameMap.hasOwnProperty(granularity)) {
      this.granularity = GranularityQuery.nameMap[granularity];
    } else {
      this.granularity = granularity;
    }
  }

  async execute(options) {
    let results = await this.query.execute(options);

    if (typeof this.granularity === 'number') {
      results = results.map(result => result.collapsed(this.granularity));
    }

    return mergeDuplication(results);
  }

  normalize() {
    const normalizedSubQuery = this.query.normalize();

    if (normalizedSubQuery === EmptyQuery) {
      return EmptyQuery;
    } else {
      return new GranularityQuery(normalizedSubQuery, this.granularity);
    }
  }

  toString() {
    return `granularity(${this.query}, ${JSON.stringify(this.granularity)})`;
  }
}

GranularityQuery.nameMap = {
  occurence: 5,
  panel: 4,
  region: 3,
  page: 2,
};

GranularityQuery.mergeDuplication = mergeDuplication;
