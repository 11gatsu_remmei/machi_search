/**
 * 検索オプション
 *
 * @typedef {Object} SearchOption
 * @property {number=} granularity AND検索やOR検索の粒度
 */

/**
 * 問い合わせ。
 */
export default class Query {
  /**
   * 非同期的に検索して結果を返す。
   *
   * @param {SearchOption} options
   * @return {Promise[Result[]]}
   */
  async execute(options) {
    return [];
  }

  /**
   * 空のサブクエリを取り除いて返す。
   *
   * @return {Query}
   */
  normalize() {
    return this;
  }

  toString() {
    return 'Query';
  }
}
