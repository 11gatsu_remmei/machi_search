import NGramQuery from './n_gram_query.js';
import EmptyQuery from './empty_query.js';
import AndQuery from './and_query.js';
import OrQuery from './or_query.js';
import ByQuery from './by_query.js';
import GranularityQuery from './granularity_query.js';

/* 問い合わせの文法:
 *
 * query = disjunction $
 *
 * disjunction = conjunction (('OR' | '|') conjunction)*
 *
 * conjunction = signed (('AND' | '&')? signed)*
 *
 * signed = ('-' | 'NOT')* qualified
 *
 * qualified = primary (
 *               (('BY' | 'by:') atom) |
 *               (('GRANULARITY' | 'granularity:') GRANULARITY)
 *             )*
 *
 * primary = '(' disjunction ')'
 *         | atom
 *
 * atom = (NOT_QUOTED | QUOTED | /["“”]/) atom*
 *
 * NOT_QUOTED = /[^-|&"“”() ]+/
 * QUOTED = /["“”]([^"“”]+ | \\["“”])+"“”/
 *
 * GRANULARITY = /occurence|panel|region|page/
 *
 * 引用符の扱いはTwitter検索を参考にした。
 * Google検索の場合はこれに加えてU+201E DOUBLE LOW-9 QUOTATION MARK („)も
 * 引用符として扱う。
 */

/**
 * トークンの型。
 *
 * トークンの種類は以下のいずれかである。
 *
 * - open_parenthesis: 開き括弧
 * - close_parenthesis: 閉じ括弧
 * - word: 単語
 * - operator: 演算子
 *
 * @typedef {Object} Token
 * @property {string} type トークンの種類
 * @property {string} value トークンの文字列
 * @property {number} position トークンの先頭の位置
 * @property {number} endPosition トークンの末尾の次の位置
 */

/**
 * スキャナーの型。
 * 文字列とスキャン開始位置を受け取り、トークン列とスキャン終了位置を返す関数。
 *
 * @typedef {function (string, number): ?{ tokens: Token[], endPosition: number }} Scanner
 */

/**
 * パーサーの型。
 * トークン列とパース開始位置を受け取り、パース結果とパース終了位置を返す関数。
 *
 * @templete T
 * @typedef {function (Token[], number): ?{ value: T, endPosition: number }} Parser
 */

/**
 * 与えられた文字列のうち、与えられた位置以降で最初の空白でない位置を返す。
 *
 * @param {string} string 対象文字列
 * @param {number} position 開始位置
 * @return {number}
 */
function skipSpaces(string, position) {
  let i = position;

  while (string.charCodeAt(i) === 0x20) {
    i++;
  }

  return i;
}

/**
 * 複数のスキャナの和であるスキャナを返す。
 *
 * 前のスキャナが失敗すると後のスキャナを試すようなスキャナを返す。
 * 前のスキャナが成功した場合は後のスキャナは試さない。
 *
 * @param {Scanner[]} scanners 元となるスキャナの配列
 * @return {Scanner} 和のスキャナ
 */
function unionScanners(scanners) {
  return (string, position) => {
    for (const scanner of scanners) {
      const result = scanner(string, position);

      if (result) {
        return result;
      }
    }

    return null;
  };
}

/**
 * 与えられたスキャナの適用を繰り返すスキャナを返す。
 *
 * 間の空白は無視される。
 *
 * @param {Scanner} scanner 元となるスキャナ
 * @return {Scanner} 繰り返すスキャナ
 */
function repeatScannerWithOptionalSpaces(scanner) {
  return (string, position) => {
    let i = skipSpaces(string, position);
    const tokens = [];

    let result = scanner(string, i);

    while (result) {
      tokens.push(result.tokens);
      i = skipSpaces(string, result.endPosition);
      result = scanner(string, i);
    }

    return {
      tokens: [].concat.apply([], tokens),
      endPosition: i,
    };
  };
}

/**
 * 記号による演算子や括弧にマッチするスキャナを返す。
 *
 * 記号による演算子はアルファベットによる演算子と異なり、
 * トークンの間に空白を必要としない。
 *
 * @param {string} operator 演算子の文字列
 * @param {string} type トークンの種類
 * @return {Scanner}
 */
function operatorScanner(operator, type) {
  return (string, position) => {
    const endPosition = position + operator.length;

    if (string.substring(position, endPosition) === operator) {
      const token = {
        type,
        value: operator,
        position,
        endPosition,
      };

      return {
        tokens: [token],
        endPosition: position + operator.length,
      };
    } else {
      return null;
    }
  };
}

/**
 * 「引用符に囲われていない語」を構成する文字であればtrueを返す。
 *
 * @param {string} character 文字
 * @return {boolean}
 */
function isUnquotedWordCharacter(character) {
  return character && !/[-()&|"“” ]/.test(character);
}

/**
 * 記号でない文字からなるトークンのスキャナを返す。
 *
 * 記号でない文字からなるトークンは空白や記号により区切られている必要がある。
 *
 * @param {string} word トークンの文字列
 * @param {string} type トークンの種類
 * @return {Scanner}
 */
function wordScanner(word, type) {
  return (string, position) => {
    const endPosition = position + word.length;

    if (
      string.substring(position, endPosition) === word &&
      !isUnquotedWordCharacter(string[endPosition])
    ) {
      const token = {
        type,
        value: word,
        position,
        endPosition,
      };

      return {
        tokens: [token],
        endPosition: position + word.length,
      };
    } else {
      return null;
    }
  };
}

/**
 * 検索文字列のスキャナ。
 *
 * @param {string} string スキャン対象文字列
 * @param {number} position スキャン開始位置
 * @return {?{ tokens: Token[], endPosition: number }} スキャン結果
 */
function atomScanner(string, position) {
  const chunks = [];
  let i = position;

  for (;;) {
    if ('"“”'.includes(string[i])) {
      i++;

      const start = i;

      while (string[i] && !'"“”'.includes(string[i])) {
        if (string[i] === '\\') {
          i++;
        }

        i++;
      }

      if ('"“”'.includes(string[i])) {
        chunks.push(string.substring(start, i).replace(/\\(.)/g, '$1'));
        i++;
      } else {
        chunks.push(string[start - 1]);
        i = start;
      }
    } else {
      const start = i;

      while (isUnquotedWordCharacter(string[i])) {
        i++;
      }

      if (i === start) {
        if (i === position) {
          return null;
        } else {
          const token = {
            type: 'word',
            value: chunks.join(''),
            position,
            endPosition: i,
          };

          return {
            tokens: [token],
            endPosition: i,
          };
        }
      } else {
        chunks.push(string.substring(start, i));
      }
    }
  }
}

/**
 * 文字列をトークン列にして返す。
 *
 * @param {string} string スキャン対象文字列
 * @return {Token[]} トークン列
 */
function tokenize(string) {
  const scanner = repeatScannerWithOptionalSpaces(
    unionScanners([
      operatorScanner('(', 'open_parenthesis'),
      operatorScanner(')', 'close_parenthesis'),
      operatorScanner('&', 'operator'),
      operatorScanner('|', 'operator'),
      operatorScanner('-', 'operator'),
      operatorScanner('by:', 'operator'),
      operatorScanner('granularity:', 'operator'),
      wordScanner('AND', 'operator'),
      wordScanner('OR', 'operator'),
      wordScanner('NOT', 'operator'),
      wordScanner('BY', 'operator'),
      wordScanner('GRANULARITY', 'operator'),
      atomScanner,
    ])
  );

  return scanner(string, 0).tokens;
}

/**
 * 与えられたトークン列において、括弧の対応が取れるように前後に
 * 括弧を入れて返す。
 *
 * 元の配列は破壊される。
 *
 * @param {Token[]} tokens 元のトークン列
 * @return {Token[]}
 */
function balanceParentheses(tokens) {
  if (tokens.length === 0) {
    return tokens;
  }

  const counts = tokens.reduce(
    (counts, token) => {
      if (token.type === 'open_parenthesis') {
        counts.unmatched_open_parentheses += 1;

        return counts;
      } else if (token.type === 'close_parenthesis') {
        if (counts.unmatched_open_parentheses > 0) {
          counts.unmatched_open_parentheses -= 1;
        } else {
          counts.unmatched_close_parentheses += 1;
        }

        return counts;
      } else {
        return counts;
      }
    },
    { unmatched_open_parentheses: 0, unmatched_close_parentheses: 0 }
  );

  for (let i = 0; i < counts.unmatched_close_parentheses; i++) {
    tokens.unshift({
      type: 'open_parenthesis',
      value: '(',
      position: 0,
      endPosition: 0,
    });
  }

  const endPosition = tokens[tokens.length - 1].endPosition;

  for (let i = 0; i < counts.unmatched_open_parentheses; i++) {
    tokens.push({
      type: 'close_parenthesis',
      value: ')',
      position: endPosition,
      endPosition,
    });
  }

  return tokens;
}

/**
 * 検索文字列を解析して問合せオブジェクトを返す。
 *
 * @param queryString 検索文字列
 * @return {Query} 解析結果
 */
export default function parseQuery(queryString) {
  const tokens = balanceParentheses(tokenize(queryString));

  // console.log(tokens.map(token => token.value).join());

  let i = 0;

  const parsed = parseDisjunction(tokens, i);

  if (parsed) {
    i = parsed.endPosition;

    // console.log(parsed.value.toString());

    if (i === tokens.length) {
      const normalized = parsed.value.normalize();

      // console.log(normalized.toString());

      return normalized;
    } else {
      return EmptyQuery;
    }
  } else {
    return EmptyQuery;
  }
}

/**
 * 積の和をパースして返す。
 *
 * @param {Token[]} tokens パース対象トークン列
 * @param {number} position パース開始位置
 * @return {?{ value: Query, endPosition: number }}
 */
function parseDisjunction(tokens, position) {
  let i = position;

  const conjunctions = [];

  let parsed = parseConjunction(tokens, i);

  if (parsed) {
    conjunctions.push(parsed.value);
    i = parsed.endPosition;
  } else {
    return null;
  }

  while (
    tokens[i] &&
    tokens[i].type === 'operator' &&
    (tokens[i].value === '|' || tokens[i].value === 'OR')
  ) {
    i++;

    parsed = parseConjunction(tokens, i);

    if (parsed) {
      conjunctions.push(parsed.value);
      i = parsed.endPosition;
    } else {
      i--;

      tokens[i].type = 'word';

      return parseDisjunction(tokens, position);
    }
  }

  if (conjunctions.length === 0) {
    return null;
  } else {
    return {
      value: new OrQuery(conjunctions),
      endPosition: i,
    };
  }
}

/**
 * 符号付き問い合わせの積をパースして返す。
 *
 * @param {Token[]} tokens パース対象トークン列
 * @param {number} position パース開始位置
 * @return {?{ value: Query, endPosition: number }}
 */
function parseConjunction(tokens, position) {
  let i = position;

  const terms = [];

  let parsed = parseSigned(tokens, i);

  if (parsed) {
    terms.push(parsed.value);
    i = parsed.endPosition;
  } else {
    return null;
  }

  for (;;) {
    if (
      tokens[i] &&
      tokens[i].type === 'operator' &&
      (tokens[i].value === '&' || tokens[i].value === 'AND')
    ) {
      i++;

      parsed = parseSigned(tokens, i);

      if (parsed) {
        terms.push(parsed.value);
        i = parsed.endPosition;
      } else {
        i--;

        tokens[i].type = 'word';
      }
    } else {
      parsed = parseSigned(tokens, i);

      if (parsed) {
        terms.push(parsed.value);
        i = parsed.endPosition;
      } else {
        break;
      }
    }
  }

  if (terms.length === 0) {
    return {
      value: null,
      endPosition: position,
    };
  } else {
    const positiveQueries = terms
      .filter(term => term.isPositive)
      .map(l => l.query);
    const negativeQueries = terms
      .filter(term => !term.isPositive)
      .map(l => l.query);

    return {
      value: new AndQuery(positiveQueries, negativeQueries),
      endPosition: i,
    };
  }
}

/**
 * 符号付き問い合わせ。not問い合わせは負。
 *
 * @typedef {Object} SignedQuery
 * @property {Query} query ベースとなる問い合わせ
 * @property {boolean} isPositive 符号が正であればtrue。負であればfalse。
 */

/**
 * 符号付き問い合わせをパースして返す。
 *
 * 符号付き問い合わせは、修飾付き問い合わせの前に否定が0個以上付いたものである。
 *
 * @return {?{ value: SignedQuery, endPosition: number }}
 */
function parseSigned(tokens, position) {
  const i = position;

  if (
    tokens[i] &&
    tokens[i].type === 'operator' &&
    (tokens[i].value === '-' || tokens[i].value === 'NOT')
  ) {
    const parsed = parseSigned(tokens, i + 1);

    if (parsed) {
      parsed.value.isPositive = !parsed.value.isPositive;

      return parsed;
    } else {
      tokens[i].type = 'word';

      return parseSigned(tokens, i);
    }
  } else {
    const qualified = parseQualified(tokens, i);

    if (qualified) {
      return {
        value: {
          query: qualified.value,
          isPositive: true,
        },
        endPosition: qualified.endPosition,
      };
    } else {
      return null;
    }
  }
}

/**
 * 修飾付き問い合わせをパースして返す。
 *
 * 修飾付き問い合わせは、主問い合わせの後に話者の指定や粒度の指定が
 * 0個以上付いたものである。
 *
 * @return {?{ value: Query, endPosition: number }}
 */
function parseQualified(tokens, position) {
  let i = position;

  let parsed = parsePrimary(tokens, i);

  if (parsed) {
    i = parsed.endPosition;
  } else {
    return null;
  }

  function operatorType(tokenValue) {
    if (tokenValue === 'by:' || tokenValue === 'BY') {
      return 'BY';
    } else if (tokenValue === 'granularity:' || tokenValue === 'GRANULARITY') {
      return 'GRANULARITY';
    } else {
      return '';
    }
  }

  while (
    tokens[i] &&
    tokens[i].type === 'operator' &&
    (operatorType(tokens[i].value) === 'BY' ||
      operatorType(tokens[i].value) === 'GRANULARITY')
  ) {
    const operatorToken = tokens[i];

    i++;

    if (tokens[i] && tokens[i].type === 'word') {
      const operand = tokens[i].value;

      i++;

      if (operatorType(operatorToken.value) === 'BY') {
        parsed = {
          value: new ByQuery(parsed.value, operand),
          endPosition: i,
        };
      } else {
        parsed = {
          value: new GranularityQuery(parsed.value, operand),
          endPosition: i,
        };
      }
    } else {
      i--;

      operatorToken.type = 'word';

      break;
    }
  }

  return parsed;
}

/**
 * 主問い合わせをパースして返す。
 *
 * 主問い合わせは括弧で囲われた「和」、またはアトムである。
 *
 * @param {Token[]} tokens パース対象トークン列
 * @param {number} position パース開始位置
 * @return {?{ value: Query, endPosition: number }}
 */
function parsePrimary(tokens, position) {
  let i = position;

  if (tokens[i] && tokens[i].type === 'open_parenthesis') {
    i++;

    if (tokens[i] && tokens[i].type === 'close_parenthesis') {
      return {
        value: EmptyQuery,
        endPosition: i + 1,
      };
    }

    const parsed = parseDisjunction(tokens, i);

    if (parsed) {
      i = parsed.endPosition;

      if (tokens[i] && tokens[i].type === 'close_parenthesis') {
        i++;
      }

      return {
        value: parsed.value,
        endPosition: i,
      };
    } else {
      return {
        value: new NGramQuery('('),
        endPosition: i,
      };
    }
  } else if (tokens[i] && tokens[i].type === 'word') {
    return {
      value: new NGramQuery(tokens[i].value),
      endPosition: i + 1,
    };
  } else {
    return null;
  }
}
