import Query from './query.js';
import EmptyQuery from './empty_query.js';

/**
 * 2つの検索結果配列をマージして返す。
 *
 * 検索結果はソートされていると仮定する。
 */
function mergeResults(results1, results2) {
  const results = [];
  let i1 = 0;
  let i2 = 0;

  while (i1 < results1.length && i2 < results2.length) {
    const result1 = results1[i1];
    const result2 = results2[i2];

    const newGranularity = Math.min(
      result1.sortKey.length,
      result2.sortKey.length
    );

    const collapsed1 = result1.collapsed(newGranularity);
    const collapsed2 = result2.collapsed(newGranularity);

    const comparision = collapsed1.compareLocationTo(collapsed2);

    if (comparision === 0) {
      // Resultをコマの集合としてみたときに、
      // result1 ⊆ result2または result1 ⊇ result2である。

      // コマの集合としてみたときに相手に含まれるResultの配列。
      const occurences = [];

      while (
        i1 < results1.length &&
        collapsed2.compareLocationTo(results1[i1].collapsed(newGranularity)) ===
          0
      ) {
        occurences.push(results1[i1].collapsed(newGranularity));

        i1++;
      }

      while (
        i2 < results2.length &&
        collapsed1.compareLocationTo(results2[i2].collapsed(newGranularity)) ===
          0
      ) {
        occurences.push(results2[i2].collapsed(newGranularity));

        i2++;
      }

      if (occurences.every(o => o.positions)) {
        occurences.forEach(o => results.push(o));
      } else {
        results.push(occurences.reduce((o1, o2) => o1.merge(o2)));
      }
    } else if (comparision < 0) {
      results.push(result1);
      i1++;
    } else {
      results.push(result2);
      i2++;
    }
  }

  for (; i1 < results1.length; i1++) {
    results.push(results1[i1]);
  }

  for (; i2 < results2.length; i2++) {
    results.push(results2[i2]);
  }

  return results;
}

/**
 * 複数の問い合わせ結果のいずれかにマッチする結果を返す問合せ。
 */
export default class OrQuery extends Query {
  /**
   * @param {Query[]} queries ベースとなる問い合わせ
   */
  constructor(queries) {
    super();

    this.queries = queries;
  }

  /**
   * 非同期的に検索して結果を返す。
   *
   * @param {SearchOption} options
   * @return {Promise[Result[]]}
   */
  async execute(options) {
    if (this.queries.length === 0) {
      return [];
    }

    const matches = await Promise.all(
      this.queries.map(query => query.execute(options))
    );

    return matches.reduce((left, right) => mergeResults(left, right));
  }

  normalize() {
    const queries = this.queries
      .map(query => query.normalize())
      .filter(query => query !== EmptyQuery);

    if (queries.length === 0) {
      return EmptyQuery;
    } else if (queries.length === 1) {
      return queries[0];
    } else {
      return new OrQuery(queries);
    }
  }

  toString() {
    return 'or(' + this.queries.map(query => query.toString()).join(', ') + ')';
  }
}
