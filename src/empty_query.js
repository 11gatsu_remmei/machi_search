import Query from './query.js';

/**
 * 空のクエリ
 */
const EmptyQuery = new Query();

export default EmptyQuery;
