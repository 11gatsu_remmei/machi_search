import Query from './query.js';
import EmptyQuery from './empty_query.js';
import Result from './result.js';
import encodeBase64Url from './base64.js';
import GranularityQuery from './granularity_query.js';

/**
 * 書籍の中でのn-gramの出現1つを表すオブジェクト。
 * ルビがあるため、「次のn-gram」は1つではなく、複数ある。
 *
 * @typedef {Object} IndexEntry
 * @property {string} book 書籍名
 * @property {string[]} location コマの位置を表す表示用の文字列の列。階層化されていて、最初の要素が最も粗い。
 * @property {number[]} sortKey コマの位置を表すソート用の数値列。辞書順にソートされる。
 * @property {string} speaker 話者
 * @property {number} position コマの中の同一話者のセリフの中での位置
 * @property {number[]} [nexts] 次のn-gramのpositionの配列。
 *                              省略時は「position + 1」1つだけからなる
 *                              配列である。
 */

/**
 * 文字列をn-gramに分割して返す。
 *
 * @param {string} term 分割対象の文字列
 * @param {nubmer} size n-gramのn
 * @return {string[]} n-gramの配列
 */
function generateNGrams(term, size) {
  const ngrams = [];

  for (let i = 0; i < codePointCount(term) - size + 1; i++) {
    ngrams.push(substringByCodePointOffset(term, i, i + size));
  }

  return ngrams;
}

/**
 * 与えられた文字列に含まれるコードポイントの数を返す。
 *
 * @param {string} string 対象の文字列
 * @return {number} コードポイント数
 */
function codePointCount(string) {
  let count = 0;

  for (let i = 0; i < string.length; i++) {
    if (!isLowSurrogate(string.charCodeAt(i))) {
      count++;
    }
  }

  return count;
}

/**
 * 与えられたコードユニットがハイサロゲートであればtrueを返す。
 *
 * @param {number} codeUnit 対象のコードユニット
 * @return {boolean}
 */
function isHighSurrogate(codeUnit) {
  return 0xd800 <= codeUnit && codeUnit < 0xdc00;
}

/**
 * 与えられたコードユニットがローサロゲートであればtrueを返す。
 *
 * @param {number} codeUnit 対象のコードユニット
 * @return {boolean}
 */
function isLowSurrogate(codeUnit) {
  return 0xdc00 <= codeUnit && codeUnit < 0xe000;
}

/**
 * 与えられた文字列において、先頭から与えられたコードポイント数後ろの
 * インデックスを返す。
 *
 * @param {string} string 対象の文字列
 * @param {number} offset コードポイント数
 * @return {number}
 */
function codePointOffsetToIndex(string, offset) {
  let index = 0;

  for (let i = 0; i < offset; i++) {
    if (isHighSurrogate(string.charCodeAt(index))) {
      index++;
    }

    index++;
  }

  return index;
}

/**
 * 文字列に対して、与えられたコードポイントオフセットの範囲にある部分文字列を
 * 返す。
 *
 * @param {string} string 元になる文字列
 * @param {number} beginOffset 開始位置(この位置を含む)
 * @param {number} endOffset 終了位置(この位置を含まない)
 * @return {string}
 */
function substringByCodePointOffset(string, beginOffset, endOffset) {
  return string.substring(
    codePointOffsetToIndex(string, beginOffset),
    codePointOffsetToIndex(string, endOffset)
  );
}

/**
 * テキストに対して、句読点を削除するなどの正規化をして返す。
 *
 * @param {string} words 元となるテキスト
 * @return {string}
 */
function stripPunctuations(words) {
  return words
    .replace(/[-、。,.!?・()「」『』"'“”〜ー—…]/g, '')
    .replace(/([^\x20-\x7F]) /, '$1')
    .replace(/ ([^\x20-\x7F])/, '$1')
    .toLowerCase();
}

/**
 * n-gramの出現を、結果に変換して返す。
 *
 * @param {IndexEntry} match
 * @return {Result}
 */
function matchToResult(match) {
  const positions = new Set(match.nexts || [match.position + 1]);

  let entries = [match];

  while (entries.length > 0) {
    entries.forEach(entry => {
      positions.add(entry.position);

      entries = entry.previousEntries;
    });
  }

  return new Result(
    match.book,
    match.location,
    match.sortKey,
    new Set([match.speaker]),
    new Map([[match.speaker, positions]])
  );
}

/**
 * 単語をn-gramを使って検索する問合せ。
 *
 * 1文字の単語はmonogramとして検索し、2文字以上の単語はbigramを使って検索する。
 */
export default class NGramQuery extends Query {
  /**
   * @param {string} term 検索する単語
   */
  constructor(term) {
    super();

    const normalized = stripPunctuations(term);

    this.normalized = normalized;

    if (normalized.length === 0) {
      this.ngrams = [];
    } else if (normalized.length === 1) {
      this.ngrams = [normalized];
    } else {
      this.ngrams = generateNGrams(normalized, 2);
    }
  }

  /**
   * 非同期的に検索して結果を返す。
   *
   * @param {SearchOption} options
   * @return {Promise[Result[]]}
   */
  async execute(options) {
    if (this.ngrams.length === 0) {
      return [];
    }

    const entries = await Promise.all(
      this.ngrams.map(ngram =>
        fetch(`index/${encodeBase64Url(ngram)}.json`).then(response => {
          if (response.ok) {
            return response.json();
          } else {
            return [];
          }
        })
      )
    );

    const matches = entries.reduce((lastEntries, currentEntries) => {
      if (lastEntries === null) {
        currentEntries.forEach(e => {
          e.previousEntries = [];
        });

        return currentEntries;
      }

      currentEntries.forEach(currentEntry => {
        currentEntry.previousEntries = lastEntries.filter(lastEntry => {
          const nexts = lastEntry.nexts || [lastEntry.position + 1];

          return (
            currentEntry.book === lastEntry.book &&
            currentEntry.sortKey.every((e, i) => e === lastEntry.sortKey[i]) &&
            currentEntry.speaker === lastEntry.speaker &&
            nexts.some(p => p === currentEntry.position)
          );
        });
      });

      return currentEntries.filter(e => e.previousEntries.length > 0);
    }, null);

    return GranularityQuery.mergeDuplication(
      Result.sort(
        matches.map(matchToResult).map(m => m.collapsed(options.granularity))
      )
    );
  }

  normalize() {
    if (this.ngrams.length === 0) {
      return EmptyQuery;
    } else {
      return this;
    }
  }

  toString() {
    return JSON.stringify(this.normalized);
  }
}
