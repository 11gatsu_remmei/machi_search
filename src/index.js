import parseQuery from './query_parser.js';
import GranularityQuery from './granularity_query.js';

const searchButton = document.querySelector('.search-button');
const resultContainer = document.querySelector('.search-results');
const searchField = document.querySelector('.search-field');
const progressMessageContainer = document.querySelector('.progress-message');
const searchCountContainer = document.querySelector('.search-count');
const granularityField = document.querySelector('.granularity-field');

searchButton.addEventListener('click', async _ => {
  searchButton.disabled = true;
  progressMessageContainer.style.display = 'inline';

  resultContainer.textContent = '';
  searchCountContainer.textContent = '';

  const granularity = GranularityQuery.nameMap[granularityField.value];

  try {
    const options = {
      granularity,
    };
    const results = await parseQuery(searchField.value).execute(options);

    searchCountContainer.textContent = `${results.length} 件`;

    for (const result of results) {
      const resultItem = document.createElement('li');
      const sortedSpeakers = Array.from(result.speakers).sort();
      const location = result.location
        .slice(0, granularity)
        .filter(s => s !== '')
        .join(' ');

      resultItem.textContent = [
        result.book,
        location,
        `(${sortedSpeakers.join(', ')})`,
      ].join(' ');

      resultContainer.appendChild(resultItem);
    }
  } finally {
    searchButton.disabled = false;
    progressMessageContainer.style.display = 'none';
  }
});

searchField.addEventListener('keydown', e => {
  if (e.keyCode === 13) {
    e.preventDefault();
    searchButton.click();
  }
});
