import Query from './query.js';
import EmptyQuery from './empty_query.js';
import Result from './result.js';

export default class ByQuery extends Query {
  constructor(query, speaker) {
    super();

    this.query = query;
    this.speaker = speaker;
  }

  async execute(options) {
    const results = await this.query.execute(options);

    return results
      .map(result => {
        const newSpeakers = Array.from(result.speakers).filter(
          speaker => speaker === this.speaker
        );

        if (newSpeakers.length === 0) {
          return null;
        } else {
          return new Result(
            result.book,
            result.location,
            result.sortKey,
            new Set(newSpeakers),
            result.positions
          );
        }
      })
      .filter(result => result !== null);
  }

  normalize() {
    const normalizedSubQuery = this.query.normalize();

    if (normalizedSubQuery === EmptyQuery) {
      return EmptyQuery;
    } else {
      return new ByQuery(normalizedSubQuery, this.speaker);
    }
  }

  toString() {
    return `by(${this.query}, ${JSON.stringify(this.speaker)})`;
  }
}
