function compareArray(array1, array2) {
  const length = Math.min(array1.length, array2.length);

  for (let i = 0; i < length; i++) {
    if (array1[i] < array2[i]) {
      return -1;
    } else if (array1[i] > array2[i]) {
      return 1;
    }
  }

  if (array1.length === array2.length) {
    return 0;
  } else if (array1.length < array2.length) {
    return -1;
  } else {
    return 1;
  }
}

/**
 * 検索結果
 */
export default class Result {
  /**
   * @param {string} book 書籍名
   * @param {string[]} location コマの位置を表す表示用の文字列。階層化されていて、最初の値が最も粗い。
   * @param {number[]} sortKey コマの位置を表すソート用の数値列。辞書順にソートされる。
   * @param {Set.<string>} speakers 話者
   * @param {?Map.<string, Set.<number>>=} positions コマ内での文字の位置を表す数値の集合。粒度がコマ以上の場合は無しまたはnull。
   */
  constructor(book, location, sortKey, speakers, positions) {
    /** @member {string} book 書籍名 */
    this.book = book;

    /** @member {string[]} location コマの位置を表す表示用の文字列。階層化されていて、最初の値が最も粗い。 */
    this.location = location;

    /** @member {number[]} sortKey コマの位置を表すソート用の数値列。辞書順にソートされる。 */
    this.sortKey = sortKey;

    /** @member {Set.<string>} speakers 話者 */
    this.speakers = speakers;

    /** @member {?Map.<string, Set.<number>>} positions コマ内での文字の位置。話者から位置の集合へのマップ。粒度がコマ以上の場合はnull。 */
    this.positions = positions || null;
  }

  /**
   * 結果の配列をソートして返す。
   *
   * @param {Result[]} results
   * @return {Result[]}
   */
  static sort(results) {
    return results.sort((r1, r2) => r1.compareLocationTo(r2));
  }

  /**
   * 与えられた「結果」とコマの位置で比較して、同じであれば0を返し、
   * 前に出現すれば負の数を返し、後に出現すれば正の数を返す。
   *
   * @param {Result} other 比較対象の結果
   * @return {number}
   */
  compareLocationTo(other) {
    if (this.book === other.book) {
      return compareArray(this.sortKey, other.sortKey);
    } else if (this.book < other.book) {
      return -1;
    } else {
      return 1;
    }
  }

  /**
   * 同じコマの「結果」を統合して返す。
   *
   * @param {Result} other 比較対象の結果
   * @return {Result}
   */
  merge(other) {
    const newSpeakers = new Set(this.speakers);

    for (const speaker of other.speakers) {
      newSpeakers.add(speaker);
    }

    let newPositions;

    if (this.positions) {
      newPositions = new Map();

      this.positions.forEach((positions, speaker) => {
        newPositions.set(speaker, new Set(positions));
      });

      if (other.positions) {
        other.positions.forEach((positions, speaker) => {
          if (!newPositions.has(speaker)) {
            newPositions.set(speaker, new Set());
          }

          const positionsForSpeaker = newPositions.get(speaker);

          positions.forEach(p => positionsForSpeaker.add(p));
        });
      }
    } else {
      newPositions = null;
    }

    return new Result(
      this.book,
      this.location,
      this.sortKey,
      newSpeakers,
      newPositions
    );
  }

  /**
   * 粒度を粗くした結果を返す。
   * 既に指定された粗さより粗い場合は自身を返す。
   * そうでない場合はpositionsはnullになる。
   *
   * @param {number} granularity 粒度
   * @return {Result}
   */
  collapsed(granularity) {
    if (this.sortKey.length < granularity) {
      return this;
    }

    const newLocation = this.location.slice(0, granularity);
    const newSortKey = this.sortKey.slice(0, granularity);

    return new Result(this.book, newLocation, newSortKey, this.speakers, null);
  }

  withLocation(location) {
    return new Result(
      this.book,
      location,
      this.sortKey,
      this.speakers,
      this.positions
    );
  }

  withSortKey(sortKey) {
    return new Result(
      this.book,
      this.location,
      sortKey,
      this.speakers,
      this.positions
    );
  }

  withSpeakers(speakers) {
    return new Result(
      this.book,
      this.location,
      this.sortKey,
      speakers,
      this.positions
    );
  }

  withPositions(positions) {
    return new Result(
      this.book,
      this.location,
      this.sortKey,
      this.speakers,
      positions
    );
  }
}
