/**
 * 文字列をUTF-8のバイト列に変換し、base64url形式 (RFC 4648 第5節)で
 * エンコードして返す。
 *
 * @param {string} text エンコード元の文字列
 * @return {string} エンコード後の文字列
 */
export default function encodeBase64Url(text) {
  // https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
  return btoa(
    encodeURIComponent(text).replace(/%([0-9A-F]{2})/g, (_, p1) =>
      String.fromCharCode('0x' + p1)
    )
  )
    .replace(/=/g, '')
    .replace(/\+/g, '-')
    .replace(/\//g, '_');
}
