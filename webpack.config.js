const path = require('path');

module.exports = (env, argv) => {
  const isDevelopment = argv.mode === 'development' || argv.mode === undefined;
  const devtool = isDevelopment ? 'eval-source-map' : undefined;

  const targetBrowsers = isDevelopment
    ? ['last 1 firefox versions']
    : ['last 2 versions'];

  const babelOptions = {
    plugins: ['@babel/plugin-transform-runtime'],
    presets: [
      [
        '@babel/preset-env',
        {
          targets: {
            browsers: targetBrowsers,
          },
        },
      ],
    ],
  };

  return {
    mode: 'development',
    context: path.resolve(__dirname, 'src'),
    entry: ['./index.html', './usage.html', './index.js'],
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
      contentBase: path.join(__dirname, 'dist'),
    },
    devtool,
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: babelOptions,
          },
        },
        {
          test: /\.html$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]',
              },
            },
            {
              loader: 'extract-loader',
            },
            {
              loader: 'html-loader',
              options: {
                attrs: ['img:src', 'link:href'],
              },
            },
          ],
        },
        {
          test: /\.(css|png|svg|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]',
              },
            },
          ],
        },
      ],
    },
  };
};
